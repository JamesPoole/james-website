---
This blog is written by [James Poole](https://twitter.com/itsapooleparty), built with my [blog-builder](https://github.com/JamesGallagherPoole/blog-builder).

Let me know you found this corner of the internet:  james at poole dot ie