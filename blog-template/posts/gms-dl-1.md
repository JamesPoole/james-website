---
title: Announcing Give Me Strength
date: 2020-10-08
categories:
  - Give Me Strength Devlog
---

![GMS](images/gms_cover_old.png)

Over the last few months, I have been developing my next project that I am calling "Give Me Strength.".

This year, I have felt the most stressed and overwhelmed than I think I have ever felt. I have been trying to come up with ways to overcome this and take control again. Writing down thoughts as they pass through my mind has been a big help for me to come to terms with what is rattling round my skull. So much shite goes through my head that I normally wouldn't take notice of but carrying out this exercise has brought up some funny things that I wasn't expecting. I wanted to do something with this and where some people have the skills to put those thoughts into song or comedy or other forms of expression, I am going to try and use them as the basis for this experimental game.

I want to do my best to create a world where you are surrounded by hundreds of characters, each representing a single thought that he is vocalising to himself. On the surface, you won't be able to make out any discernible sentences but by exploring further into the world you can start to create a picture of what is going on and be led down different trains of thoughts. This is the general idea and I will play with it a bit to create different scenes that delve into different types of thoughts. I am not exactly sure how it will turn out but I want to give it my best shot to see how it will take form in the end.

So where am I at with development? I have been working it whenever I get the chance since July. It will probably be only about a ten minute play so I am setting myself a deadline to be finished working on it by early December so I won't be sinking any more of my life into something that may well be shite.

Thank you very much for the support. Shoot me a message wherever if you have any questions about it! There will be more bits and bobs to follow!