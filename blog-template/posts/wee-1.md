---
title: Announcing Wee Boats
date: 2023-10-12
categories:
  - Wee Boats Devlog
---
Whats going on? We're making a new game! Wahey! And there's a crew of us putting this one together! [Laura Ryder](https://lauraryder.bandcamp.com/), [Brendan Hewson](https://brentendo.itch.io/) and myself are working on this and we're going to make something class!

The game is called Wee Boats! I'm so hyped with how its shaping up and I want to share more about how it's coming along! 

![Wee Boats](images/orange.png)

![Gameplay Image 1](images/wee_boats_1.png)

Wee Boats will be there to provide a short, calm experience that is more an ambient bob about a body of water than a game. It's pretty simple. You control a small ship and can explore a world that is full of character and little surprises. Your only other control is that you can honk. Honk to yourself. Honk at other boats. Honk at lighthouses. The potential is endless! We are also creating a logbook that will keep track of the things that you have honked at. A nice record of your time boating at a wee scale.

![Gameplay Image 2](images/wee_boats_2.png)

Over the next few months, we want to explore different aspects of the making of the game as it develops. I find it tough opening up about the development because its exposing this small idea out to the world. Some of my current concerns are:
- Is it cool enough to get people excited?
- Will we be able to get it finished?
- Will I be able to keep up posting about it?
- Will I have enough interesting things to talk about through the process of development?
- Aaaaaaaa

However, let's give it a whack anyway!

As an experiment and a challenge, I've opted to use Bevy as the game engine and write it as Rust. Its been mostly great and I'm now even happier with the decision after all the Unity shite. This is an open source engine and we own the game! I'll talk more about Bevy in another post because it's great.

So where are we at today?
We first started programming the game in March 2023. 6 months, feck. There has been a couple of main areas worked on so far:
- Building out the systems to display the sprites using a method called sprite stacking to create a pseudo 3D look. 
- Building out some small tools to make development flow a little easier.
- Adding animations and interactions for birds and structures.
- Adding a very primitive logbook and UI.
- Testing out audio directions for the soundscapes.

What's up next?
- Adding support for underwater creatures and things to pop up and say hello.
- Making the logbook look nice.
- Building more world!
- Adding interactions to all of the structures and animals added so far.

Cheers for reading. Sign up to the [newsletter](https://mailchi.mp/18efdca94182/newsletter) to receive the odd update about my work and I'll let you know when you can get wee boating!

You can also find the pre-release game page on [Itch](https://jamespoole.itch.io/wee-boats) where you can follow the development.
