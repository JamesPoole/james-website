---
title: Give Me Strength Devlog 3
date: 2021-05-15
categories:
  - Give Me Strength Devlog
---

I have been working on Give Me Strength for almost a year now and we still have a small way to go.

The bulk of the groundwork was done in 2020 and 2021 has been focused on improving and refining the experience of playing the game. I have left my last job in the last few weeks so I now have a couple of months where I can focus my time on the things that I have wanted to work on with this. 

We are wrapping up the last things like final bug fixes and getting the promotional material ready for the game and we can then push it into the world.

There is also a phenomenal original dynamic soundtrack from Laura Ryder that will be in the game and its going to be awesome!

# New Projects

I am currently thinking about what I want to work on next once I can get Give Me Strength out of my head. I have a couple of thoughts for prototypes to try out and see what might work. I would quite like to make this work more public to let anyone who is interested in the process see how I may go about things.