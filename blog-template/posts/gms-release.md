---
title: Releasing Give Me Strength Today
date: 2021-08-15
categories:
  - Give Me Strength Devlog
---

![GMS](../images/gms_release.png)

Proudly made in Limerick City, Ireland, I am delighted to say that Give Me Strength is out today, August 14!

I am constantly struggling with myself when I am making games or anything creative and this piece has ended up being about that. It is a short, experimental game where the player can go about the place and be barraged by my voice and the most amazing soundtrack from Laura Ryder.

It is completely different to anything I have made before and I believe that it is a very unique experience. Its weird and it won't be for everyone but if you would like to try out a 10 minute weird game, I hope you give it a go and get something from it!

The game is available on [itch.io](https://jamespoole.itch.io/give-me-strength) and you can find more information on [my website](http://james.poole.ie)

It was supported by the Limerick Culture and Arts Office and Creative Ireland.
