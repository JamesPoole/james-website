---
title: Wee Boats is a Short Game
date: 2025-01-06
categories:
  - Wee Boats Devlog
  - Game Development
---
You can finish Wee Boats in under half an hour. You can spend longer with the world if you would like but you can get the full experience in a short amount of time.

Part of this is to make it realistic for a small team to complete a project like this. Right now, it would be almost impossible for me to create something of a larger scope even with infinite time. The drive required to bring a game project to its completion and release is all too easy lost at some point along the way.

Thankfully, this constraint goes hand in hand with the kind of games that I want to create. It allows for creating games that are concrete experiences. It allows us to focus on a small amount of ideas and explore those in a unique way. The games are therefore accessible to more people because it doesn't require a lot of commitment or complex learning from players to play the games.

Games can be 5 minutes long. If that's the amount of time for the experience to be felt then that's perfect.

Games don't have to make you feel something intensely to be valid. They can be subtle. Portray a random memory that you want to capture or something like that.

Games don't have to have an objective or a puzzle to complete. Gway with that.

Wee Boats is designed to be a short moment of exploration. A time of surprises that hopefully give you a couple of smiles, laughs and "aha thats gas!" moments while you play.

Others have written about the subject much better than I can. Some of my favourite writings on the topic are:
- [Stuffed Wombat](https://stuffedwomb.at/the_small_game.html)
- [John Tryer (Link seems to be down :( ))](http://jtroyer.me/)