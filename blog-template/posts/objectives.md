---
title: My Objectives
date: 2024-04-09
categories:
  - Software
---

- To be able to be expressive through software.
- To be in control of the software that I write.
- To understand the software that I write.
- To create software that is sustainable.
- To have the software that I write survive a long time.
- To share what I learn with others so that we can all gain more power together.

(Created 9th April 2024)

Recently, I have been going down all sorts of rabbit holes. The Indie Web, Gopher, Gemini, Solar servers, Programming languages like Forth, Uxn and Joy, microcontrollers and loads other things. These all pull me away from the things that I *should* be working on which makes me go "ahhhh!" but they all seem so exciting in the moment (and I realise how boring this all sounds)!

I was trying to take a step back and figure out why I get so enthusiastic about these topics which lead me to come to the list of objectives up above.

I feel like the direction of most modern technologies is moving in a way that is opposite to almost all of these objectives. We are losing control over our software and hardware. Software and hardware is becoming needlessly complex. Our software and hardware is very quickly becoming obsolete. It's important that we push back against this and be in the driving seat of the technology we use.

When I go into these rabbit holes, instead of coming out of them and losing whatever I find to the ether, I would like to document what I learn and share them in case others find anything interesting in them.

All the best

### Some Neat Readings
- [Weathering Software Winter](https://100r.co/site/weathering_software_winter.html)
- [Low-Tech Magazine](https://solar.lowtechmagazine.com/about/what-is-low-tech-magazine/)


