---
title: Do Your Own Research (Feck What Jonathan Blow Thinks)
date: 2024-12-09
categories:
  - Software
  - Game Development
---
## The Shite Talk

I listen to far too much programming shite talk on the internet. Twitter opinions. YouTube opinions. Hacker News opinions. The whole feckn lot. I feel like I am constantly steered by it. Most of the shite talk comes from people with far more experience than I do and some of it is probably very valid. But also, who knows? It could be a load of useless nonsense.

Even if I can pull out the most truthful and wise morsels of signal from the noise, how good of a programmer am I ever going to be if I just parrot off what I have read online? Not very good probably. I'm about 2 questions away from not knowing what I'm talking about because I haven't done my own research.

I often feel guilty or ashamed for the things that I try out and work on. Guilty because it doesn't line up with what I've read someone say online. It's probably "wrong" or I am using "the wrong tool for the job" or that my architecture is not solving the problem optimally or some shite like that.

## 2 Quick Examples

1) I learned about ECS a few years back and wanted to try out the concept. However, throughout the experience I am reading [tweet](https://x.com/Jonathan_Blow/status/1427358365357789199) after [tweet](https://x.com/Jonathan_Blow/status/1427357521371549705) about why ECS is a waste of my time. The thoughts are again, probably valid, but then I get more decision paralysis when my brain starts telling me that I'm wasting my time. But the point is to learn about ECS and what is good and bad about it. Ignore Jonathan Blow and do your own research.

2) Recently, I have seen some cool things with using Ruby for game development and I want to try it but DragonRuby in particular seems to have some neat things going on (fast iteration time, hot reloading, neat live console, apparently Ruby is nice to write). But by even writing that I feel like I will be forever put in the game developer bin. Who on earth would make a game in a slow yoke like Ruby? But I'm going to hold fast and follow my enthusiasm. Maybe it will lead to a new world of productivity. Or maybe I'll find some dealbreaking cons for me. Whatever happens, I'll have reached my own conclusions.

## Its OK

This little piece is a reminder to myself that its OK to go do the stupid thing. I'll learn my own way, using real experiences to form opinions and build my preferences. And who knows, maybe that will lead to some other discoveries that online ranters aren't going to see because they are too busy (Jonathan) blowing hot air up each others arses.

Feck Jonathan Blow for his know it all rants condescending on us all as if we are thick eejits. I'm still going to listen to the bollix because he is class but I am also not going to feel bad for trying out what he thinks is stupid. I'll learn me own way what works best for me and if I learn from experience why something doesn't work, then that's also class and I now have another bit of tested knowledge in the bank.

Dog bless