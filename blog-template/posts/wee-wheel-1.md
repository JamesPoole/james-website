---
title: Introducing the Wee Wheel!
date: 2024-12-26
categories:
  - Wee Boats Devlog
  - Game Development
---
Over the last few months, we have brought early versions of Wee Boats to two different events where it would be playable on an exhibition floor. This poses some challenges to make the game fit the environment. I personally have a tough time engaging with games or interactive pieces in exhibition situations. 

I walk up to a game...
- What are the controls? 
- Is the game in the middle of somebody else's previous play session? 
- Do I need to go through a tutorial?
- What is the time commitment?
- Am I able take part without blocking out those around me?

With all these small mental barriers, I often will pass the games by with the aim of coming back when I am in the zone. I'm not sure if others feel the same way but if its something I definitely have noticed in myself at game festivals so maybe we can try and reduce that feeling for people walking past Wee Boats.

With this in mind, how can we make Wee Boats shine at events? We want people to be able pick it up at any point and feel like they can immediately take part. No controls to learn. No backstory or context required to understand whats going on. Just go boating and get lost in the exploration that comes!

# Bring in the Wee Wheel!

The largest part of addressing this is with our handmade ship's wheel called the Wee Wheel! For exhibitions, instead of using a keyboard or a game controller, Wee Boats has been playable with this papier mache wheel.

We have found that people understand how to play from just seeing this at a distance. The goal is to signify that there are no complicated controls to learn, just grab the wheel and you are gaming! This has been extremely effective and has brought us a lot of joy seeing how people play with the game.

Laura and Brendan made the first version of the Wee Wheel that we brought to Berlin. This is us posing with our work of art.

![](images/wee_team_amaze_dither.png)
![](images/berlin_wheel.jpg)
Due to logistical issues with getting this wheel to Norway, I made another version of the wheel for the Piksel Festival. This means that now at the time of writing, there are two Wee Wheels in existence! What a time to be alive!

![](images/piksel_wheel.png)

## Handmadeness

The Wee Wheel is unashamedly toy like. It is made of cardboard, paper, masking tape and glue. It is playfully painted with acryllic paints and fat brushes. This is counter to the "gamer" style that computing is often associated with. RGB lights, hard pointed edges, neon green. Let's bring cardboard and duct tape into computing! DIY Punk computing! This scrappiness also is a signifier of a low barrier to entry and that this is something for everyone, not just "gamers".

![](images/piksel_wheel_wip_1.png)

In an upcoming piece, I will write a bit more about how we actually made the wheel. Little bit of techin' and a little bit of paperin'.

Wee Boats is not out yet but you can sign up to the newsletter [(here!)](https://mailchi.mp/18efdca94182/newsletter) to get a heads up when its coming out and you can check out the game page on [Itch.io here](https://jamespoole.itch.io/wee-boats)