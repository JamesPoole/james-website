---
title: Learnings from Playtesting Wee Boats
date: 2024-06-23
categories:
  - Wee Boats Devlog
---
Development on Wee Boats continues! Over the last few days, I have been asking a few friends to try out the game to see how they experience it. The great bit is seeing people having fun with the game. Whew! Great! The not so great bit is that the process has been highlighting some gaps in the onboarding of the game. I am going to try to come to terms with it in this post!

![](images/Pasted%20image%2020240623130332.png)

## I have done a wupsie

I haven't been thinking enough about making sure players know how I would like them to play. Saying that, I don't want to force a style a play, in fact quite the opposite! It's amazing when players carve out their own game or try things I never would have expected. But what I absolutely don't want is for the player to be confused as to what is going on because I didn't explain it to them well enough. The player should know what tools they have available to them and how they can be used. Once I am happy that I've communicated those things, then the player is welcome to feck about and break all the rules that they want!

Wee Boats is simple and I probably was hoping that that simplicity would sort itself out and players would just understand everything from the get go. But the reality of a computer game is that there is a laptop keyboard there with about 100 keys and about 4 of them are useful to this game. Which ones are they? How are they used? Another reality is that the player can go wherever they want from the start of the game which opens it up for infinite possibilities as to what they see first. Often designers create boxed-in tutorial areas where the player learns how to play the basics of the game before setting off on their adventure. However, I want to try and avoid this because I feel like whatever I do there will feel heavy handed and forced. I want our world to feel open for exploration right from the start.

## A note on Dredge

A game that I feel did this very well is Dredge. You spend time in the initial area, learning the ropes and you are kept there in a natural way because the outer world seems terrifying and your boat starts off as a piece of shite. Once you do break out of that tutorial zone, it feels like a natural part of the world. It is your safe home that you can return back to. I find it impressive that it isn't a tutorial area tacked on to the side of the game like these things often can feel like.

## What have I been at?

I have found these onboarding communication problems to be just as hard, if not harder than the various technical problems posed by making a game. And I think it is because of this that I have been procrastinating dealing with them or even avoiding looking for them in the first place. I find it much easier to look for new fun things to add on to the game like "ooh lets add a social feature" or "ooh lets add multiplayer" or "ooh lets add a choose your own boat feature". This is also dangerous because for each new thing added, that is a new thing that needs to be explained and understandable.

## Let's fix this!

I should have been testing those initial moments a year ago! And for future projects I will defo be keeping this more in mind. For now, I want to give this my best shot at fixing the problems that I am seeing before we call the game done. I have a list of about 5 things that are often problems at the start of the game that we need to sort out. I have some ideas for some but for others I am going to need to spend some time thinking! Let's go!

You can find the game page here which is where it will be downloadable when it's released: [Wee Boats](https://jamespoole.itch.io/wee-boats)
