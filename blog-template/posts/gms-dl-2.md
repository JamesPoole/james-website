---
title: Give Me Strength Devlog 2
date: 2020-11-22
categories:
  - Give Me Strength Devlog
---

![GMS](images/cover_2.png)

It is November and I am about 5 months into the development of my third game, Give Me Strength.

I have built out the chapters for the game and it is playable from start to finish although in not a very complete form. It is looking like it will be a short 5 to 10 minute playthrough. I would like if it was a bit longer but my goal now is to make that time worthwhile and provide something new for people to experience that they may not have seen before.

Each chapter has a concept that belongs to it and I am working to capture those concepts as best I can along with fixing the bugs that come along that.

The audio is going to be core to the experience and is still in the planning stages. We are putting together a plan for this and will start the recording in about a weeks time. I have some great help from friends in this department so I am excited (while nervous) to get stuck into that.

At this point I really have zero idea if this is something that I am going to be proud of or embarrassed about. It is short, weird and not a traditional game.

    “The moment that you feel, just possibly, you are walking down the street naked, exposing too much of your heart and your mind, and what exists on the inside, showing too much of yourself... That is the moment, you might be starting to get it right.”

I heard Neil Gaiman saying this recently in a podcast and it is what I needed to hear at this point of development to keep me going.

I am committed to finishing this thing just to be able to say that I weathered the doubt and shipped the work. It may be awful but what is important to me right now is that I bring it to a conclusion that I am happy with. Hopefully people will enjoy it but if they don't, hopefully they will like the next thing!

Expected release time is January 2021 and I will keep ye posted with the progress in the meantime.