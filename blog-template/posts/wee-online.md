---
title: Online Boating
date: 2024-10-28
categories:
  - Wee Boats Devlog
---
Wee Boats is rocketing to the onlines! Buckle up...Its gonna get mildly social!

We are still working on getting Wee Boats ready for release. It's looking like it could be early 2025 at this stage but we'll see how we get on! We have been working on some online components for the game so I thought I'd give a little overview of them here.

First up, it will be possible to cruise around with other online boats! Oh god ya. Bring a friend or two along for the journey to explore the world! Encounter other captains making their way through the oceans. Get out of the way Sea of Thieves, Wee Boats is coming to town. We are not adding an in game chat but you will be able to honk at each other repeatedly and constantly. Sure what else could one ever need?

Secondly we have our messages in bottles. This is a way of ephemerally communicating with other future boaters. Find messages left by boaters past and cast out your own messages to wish future boaters well. Maybe pass on a secret that you have discovered?

![](images/ring.png)

It is important to us that our games can be played long into the future so Wee Boats isn't dependent on the online servers being online in order for the game to work. If we need to bring the servers down for whatever reason in the future, you will still be able to experience the entire game without these online components. The internet folk like to call this local-first computing I do believe.

[Find the game page here and give it a follow](https://jamespoole.itch.io/wee-boats)

[Sign up the newsletter here for the odd update](https://mailchi.mp/18efdca94182/newsletter) (I still haven't mailed about anything. Its only for the big updates!)

Cool, thats it for now. See yis soon!
James