---
title: Bookmarks to Cool Places on the Web
date: 2023-11-17
categories:
  - Bookmarks
---
I have discovered so much through finding people's personal websites and following to what they link to. This is my version of those pages with a list of some of the sites that have formed me. This is a living list that I am adding to.

## Tadi
- [Todepond](https://todepond.com) - (So much from Lu's work is amazing. Sharing scrappy fiddles. Working on small, interesting projects. Working in the open. Live programming investigations. Wikibloggarden.)

## Code
- [Linus Lee](https://thesephist.com/)
- [Tumble Forth](https://tumbleforth.hardcoded.net/)
- [Uxn](https://100r.co/site/uxn.html)
- [Julia Evans](https://jvns.ca/)
- [Dave Gauer](https://ratfactor.com/) - (Cool engineer with lots of great writings and a super About page. Great piece on Forth)

## Game Development
- [Elias Daler](https://edw.is/) - (Building games from scratch. Writings on C and Lua)
- [Pikuma](https://pikuma.com/blog) - (Understanding game development fundamentals and mathematics)
## Permacomputing
- [XXIIVV](https://wiki.xxiivv.com/site/home.html)
- [Hundred Rabbits](https://100r.co)
- [Kartik Agaram](http://akkartik.name/feed) - (Building Freewheeling Apps using Lua and Love2D)
- [Lee Tusman](https://leetusman.com/nosebook/feed.xml) - (Artists and Hackers Podcast. Interesting writings.)

## Handmade Web
- [html.energy](https://html.energy)
- [Jes](https://j3s.sh/)
- [Laurel Schwulst](https://laurelschwulst.com/)
- [Handmade Web](https://handmade-web.net/index.html)
- [The HTML Review](https://thehtml.review/)
## Low Tech
- [The Web's Obesity Crisis](https://idlewords.com/talks/website_obesity.htm)
- [Low Bandwidth Guidelines](http://www.aptivate.org/webguidelines/topten.html)
- [How to Build a Low Tech Website](https://solar.lowtechmagazine.com/2018/09/how-to-build-a-low-tech-website/)

## Blogs
- [Marginalia](https://www.marginalia.nu/log/)
- [Derek Sivers](https://sive.rs/)


