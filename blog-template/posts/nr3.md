---
title: Game Number 3
date: 2020-08-01
categories:
  - Give Me Strength Devlog
---

There is another game in the works! This time I have no clue if it is going to be any good at all. It is a slightly larger project than my previous outings so I am still trying to figure out how to go about it. I haven't fully wrapped my head around it yet but my hope for writing down these bits is to help clear my thoughts.

The goal of the game is to create an interactive environment to explore the mess of thoughts that we get on a daily basis. I can get easily carried away with negative or emotional thoughts and this is a project to try and make me pause and take control of some of them.

![Dancing Character](images/post_1_dance_1.gif)

While that is my goal and direction, how it will take shape as an interactive experience, I am still unsure. The initial brainstorming of a project can be fun and inspirational as you go off in different directions coming up with new and exciting ideas. Sitting down to make that a reality however is what I am finding so tough.

 What is the game at its core?

 How is the world going to look and feel?

 What will the players find engaging in the world?

![Dancing Character](images/post_1_dance_2.gif)

I am currently creating a prototype of the world that is in my head to try and help answer some of these questions. A small space that contains the core ideas to be explored and give a representation of what the full game will be like. From this, I can get a feel of what works and what doesn't. I still have questions that I haven't fully solved but my hope is that by building upon this prototype, new ideas will come and go and one or two of them might give me something that will bring me forward.