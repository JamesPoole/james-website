---
title: How We Arrived at Stuart's Perfect Day
date: 2024-10-13
categories:
  - Stuart's Perfect Day Devlog
---
We are a team of three based in Norway. This was our first game jam together so we are far from being seasoned jammers with effective plans and routines (We have none! :D). Our aim was to have a bit of fun and see if we could come up with something unexpected over the weekend.

The only prep we had done was from Fredrik, our in-house designer and artist, had done some concept sketches to get a feel for an art style that we wanted to aim for. These were the concepts:

![](images/concepts.jpg)

Saturday morning arrived, along with the theme and we started throwing out ideas around the Tiny Creatures.  

## Quick Prototypes

Fredrik also happens to be lethal at the oul Construct 3. He can bang out 2 prototypes before you can say "So what engine are we using?". While we sat in the first coffee shop of the weekend, he took one of our early concepts and got a quick version going. We could then see how that played, riff off it and do a good few "what if"s and see where it would lead us.

We went through about 3 or 4 of these.

A platformer...
![](images/Squares0.gif)

Another platformer...
![](images/Squares1b.gif)

A circly platformer...
![](images/Circles0b.gif)

An arty platformer...

![](images/Pearl.gif)

...and they all could have gone somewhere but they weren't lighting our world on fire. Instead of choosing one and going for it like we should have, we decided to park them and try something else.

We returned to Fredrik's concept sketches in search of inspiration and decided to capture the boat sketch in something simple that incorporated some tiny creatures. Fishing game! Lets go! Suddenly this brought life back into the gang imagining the world that could belong to the image.

Saturday evening, in the dark corner of a cafe/bar situation in Bergen, we set off at trying to figure out this world of Stuart and started bringing it all to life.

![](images/vagal.jpg)

At this point we are almost a day down with nothing substantial towards a submit-able project. However, without being able to try out those different ideas quickly, we wouldn't have found Stuart. Being able to test the ideas in reality as opposed to concepts in our heads or on paper was beyond helpful.  

For the rest of the jam, we needed to scope down even more than normal since we had about 1 day to get the rest done but we were still able arrive at something that brought us joy!

You can see the Ludum Dare page for Stuart's Perfect Day here: [https://ldjam.com/events/ludum-dare/56/stuarts-perfect-day](https://ldjam.com/events/ludum-dare/56/stuarts-perfect-day)  

And here is a timelapse of Stuart and is world coming to life...

![](images/StuartsTimelapse.gif)