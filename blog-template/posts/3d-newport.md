---
title: "Project 3D Newport"
date: 2021-05-28
categories: 
  - 3D Art
---

# Project Baile Uí 3Dacháin Intro

I am trying to build some ideas for what I want to work on next but its not coming to me very easily. Instead of sitting around and feeling bad for myself while I have no class ideas, I'm going to work on this and hopefully it will trigger some ideas as I work on it.

## What is it?

I want to build a 3D environment that you can walk around and explore. It could be a scene that I use in a future game or it might not. The point is just making the thing.

I am going to build a town because square buildings are a good start I reckon for someone who is new to 3D modeling. More specifically, Main Street, Newport in County Mayo is going to be my target. I am using a real town so that I have no excuse to stop working on the project for any lack of inspiration reasons. If I am feeling like I don't know what to do, I just take the next building and model that in its basic form. It will be a stylised interpretation of the town or in other words, very simple and blocky.

I will be using Blender to model my town and I will use Unity if I make a playable walkthrough version of the town. I would quite like to use the same scene to create different feelings like Newport if it was Sunny LA or if it was Silent Hill. 

I don't know why Newport hasn't been the location used for many video games previous to this. Even the name sounds great even to say. Listen. Newport. Wow!

## What do I want to get out of this?

Primarily, this is an exercise for me to work my way out of being stuck for ideas. Second reason would be that I have only ever dipped in and out of Blender. This would be a larger project for me to get stuck into. There are a few concepts like Texture Mapping that are core basics to working in 3D and I don't have a clue how they work. This would be an opportunity to get some exposure to those.

Also I find it very difficult to post my work to Twitter. This will give me something to post consistently about. Most posts are going to be extremely boring given that some buildings are going to just look like cubes but that will be part of the fun of it.

## What does Newport look like?

![town](images/town.png)

Phenomenal

## Some References

![ref1](images/ref1.png)

![ref2](images/ref2.png)

![ref3](images/ref3.png)

![ref4](images/ref4.png)

## Rules

1. I must use a colour palette which has no more than 4 colours.
2. I must post each new building and the resulting town scene to Twitter as I complete it.
3. I don't need to strictly stick to the real buildings of Newport. If some wave of inspiration comes to change up Walsh's Shoes and turn it into a medieval castle, I'll go with it. It could be the thing that triggers my next game.
4. I must spend no longer than 30 minutes on one building. Capture the essence of it and move on.
5. I must open source the model and any files associated with it.