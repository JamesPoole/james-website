---
title: Day One Live Coding
date: 2024-12-23
categories:
  - Live Coding
  - Software
---
Over the last few months I have been becoming more aware of the live coding world.

The first algorave that I went to was at [Piksel Festival](https://piksel.no/) this year.

It was unreal! Improvised pieces, prepared pieces. Everything welcome and everything bringing something different.

I am writing this at a point where I think its unreal, but have not tried it out myself yet.

# Why am I drawn to it?

- It can be chaotic. 
- It can be structured.
- Experimentation is encouraged.
- Collaboration is encouraged.
- It is vulnerable.
- It is free jazz.
- It is programming for fun.

# Tools I am trying out

## [Strudel](https://strudel.cc)

A web based tool for live coding music!

I've tried it for the first time this morning and this was the ([scrappy fiddle](https://www.todepond.com/pondcast/demo/)) [result](https://strudel.cc/?8dDojaQCqLdB)

(Here are the two lines in case the link dies at some stage:)
```
$: n("[4*<4 1 2>] <6 [~ 6]> ~ 5").sound("jazz").scope()
$: s("cp hh sd hh").bank("<RolandTR909 RolandTR505>").fast(2).mask("<0 1>/2")
```
## [Hydra](https://hydra.ojack.xyz)

A web based tool for live coding visuals!

## [Flok](https://flok.cc)

A web based tool for collaborating with others using Strudel, Hydra and some others.
