---
title: Submitting Wee Boats to A Maze 2024
categories:
  - Wee Boats Devlog
date: 2024-04-03
---
## 1 Week Before Submission

It is currently January 24 and we have a week left before the submission date for A Maze. A Maze is a small festival for experimental games and interactive art that I have attended the last few years.

Its very easy to feel like nobody is out there or trying your games when you release them online. In my experience, even with a nice amount of downloads, I hear very little back from those who do try out the games. Did they have a nice time? Did they hate it? Did it inspire them to do something afterwards? I'm not sure, because even if they did, that get's lost over the internet.

This is why I value the likes of A Maze because it brings like minded people together, to play cool games and share experiences. It felt like real people were playing the things that we made :O ! 

This is why bringing Wee Boats to the festival has been a big goal since the outset of the project. We want to get it into people's hands and feel how they experience it! However its not something that we can take for granted as we must first submit and be nominated for an award to be able to exhibit a project. It's a long shot with the quality of the games there but lets see how we go.

The game is not finished so we are doing our best to pull together a demo that we can submit that will portray the feel of the game. Its so cool to see everything coming together after about a year of the project looking quite janky in some way. We have lots of ingredients all over the place and now is when we start to put them together into a neat little package.

We have music coming in, logbook sketches and the world continues to be filled with life and new things to find!

Submitting it is also forcing me to think about a lot of the things important for releasing a game that I have not needed to think about over the last year. 
Some of these are: 
1. Figuring out how to make builds for Windows, Mac and Linux.
2. Coming up with an app icon.
3. Making sure the window opens in the right way in full screen.
4. Explaining the controls.
5. User testing D:

Currently, I have managed to get a working build for Windows and for Linux. Mac has decided that it won't cooperate. This is an important one to have though so I am going to pray to the Apple gods and give this another go in the coming days.

## 2 Months After Submission

It is now April 3rd and I am back writing again to give a little post-mortem of the last months. We went hell for leather in the run up to the submission deadline. It was a week of late evenings Wee Boating. Its crazy the number of things that need to get done in order to prepare a game for any kind of a release, even for this vertical slice demo.

There is a lot of hidden work in getting a game ready for a new person to pick up it up and have a fun time with it. Creating builds, reducing build size, explaining controls, last minute features, testing on the different platforms, fixing bugs.

The Mac version of the game was being a nightmare so we threw that in the bin and went to focus on just the Windows version and getting that as robust as we could.

And of course, after a pretty chill year of dev with very few bugs, the last day is when a couple of humdingers came to say hello. The game would give a white screen and nothing else sometimes and sometimes the music wouldn't start. Super!

We managed to submit a demo that we were happy with but it fairly wrecked us! If you look at the picture below, you can see us scrambling in those dark green days at the end of January and then it goes dead in the subsequent weeks where I couldn't face the computer again. I'm still recovering from it to be honest and still trying to get back on the saddle.

![Github Activity](images/Pasted%20image%2020240403215859.png)

In the end we didn't get nominated for an award which is a shame but it is to be expected because the game is still in quite an early state. While its charming now, I don't think it has that punch that a properly polished piece has yet. We do however have a 2 hour demo slot at the festival to show it off so that will be another milestone to aim towards and get some people trying it out.

## Working on a New Area!

As a part of trying to wrap up development on Wee Boats, we are working on what we want the end game to be like. The goal is to have a nice surprise for those who stick around until the end and have it feel fulfilling in a Wee Boatsy way.

I am not going to spoil what that is yet but we have something in the works that we are pretty hyped about!

## Working on the Wheel!

![Picture of a Papier Mache Wheel](images/wip_papier_mache_wheel.jpg)

As a part of our on site demo, we have been constructing some of our own hardware to control the game! One of the pieces is a ships wheel to mount to the desk. The logic for controlling the boat is different to how we have so far been doing it with the keyboard so recently we have been doing little bits with getting that wheel experience nice. More specific info and videos will come for this soon!

## Starting work on the online components!

While Wee Boats will be fully playable offline, we are planning on adding some optional online elements to add extra depth to the experience. The first part that we are working on is adding messages in bottles! 

The idea is that you might come across a bottle from another player, dropped sometime in the past and see what their little message to the world was. And likewise, you can then drop a thought into the Wee Boats ocean for a future sailor to find.

We are programming our server and client code for this at the moment to save and update the messages. Its going well so far and we will then be moving on to implementing the UI and in game assets for it!

## That's all for now

We have about a month until the demo in Berlin so the focus will be on bringing the game further and then polishing like heck again before that. Hopefully I will get some more pieces written for it because I have a growing list of things I want to share! See ye soon!
