---
title: How we are using Sprite Stacking for Wee Boats
date: 2024-02-11
categories:
  - Wee Boats Devlog
---
Wee Boats was originally born out of wanting to try out an interesting technique that I had seen online a few years ago. I have seen it being called Sprite Stacking so let's use that name here!

Sprite Stacking is a way of creating pseudo 3D assets, using 2D horizontal slices that are stacked on top of each other. Especially when moving around, if you update the angle of each slice, it creates a convincing depth effect as you can see the asset from all sides.

Before I start to show any visuals, I want to say that all of the art used in this post was made by Brendan Hewson who is a part of the Wee Boating team!

Here is an example of what it looks like with one of our little boats:

![Boats Moving Video](images/sprite_stack_2-2024-02-11_10.56.30.mp4)

It looks beautiful! :')

## How Do We Make Our Wee Boats?

There are so many options here with how you could go about this! I am going to highlight how we are doing it but don't let that limit you! See what mad ideas you have!

Firstly, we make our sprite slices using Aseprite as if we were making an animation. Each frame represents a slice of our model. Each slice needs to be drawn individually which isn't the easiest to do. If you turn on onion skinning it can help to see the previous and next frames. When we are ready to try out our creation, we then export this as a sprite sheet that we can use in the game.

Here is an example of an Aseprite project:
![Aseprite](images/slice_aseprite.png)

This is an example of an exported sprite sheet:
![Sprite Sheet](images/new_boat_1.png)

And then the game stacks these sprites to create our pseudo 3D boat:

![Boat Honk Video](images/sprite_stack_1-2024-02-11_10.55.31.mp4)

Here is a honk action shot that shows the slices slightly further apart:

![Honk](images/honk.png)
## Working with Challenges and Limitations

While I find this method quite charming, it also comes with some challenges. Not even pseudo 3D methods come without cons god dammit.

It can be difficult to have an overview as to what the 3D model looks like while you are drawing the layers in Aseprite. We have found external tools that let us preview the model separately which has improved the workflow a lot but is still an extra step. The best one for us has been Jon Topielski's [Stacked Sprite Viewer](https://jontopielski.itch.io/stacked-sprite-viewer).

Another challenge is that it can be difficult to model little details using this system since you are working with cross section slices. We have been able to work around this by using the top most layer in some structures as a normal 2D sprite. This can be drawn nicely and add character to the game.

Animations are another one that will be challenging with Sprite Stacking! Animating across slices would be a big pain and it was one that we didn't try solving. We have worked around it by using simple animations transforming the slices themselves. An example of this would be the bounce that the boat does on a honk, shown in the videos above. I think its pretty cute! Another workaround here was using that top most layer of the structures saved for a traditional 2D sprite. We made a system for animating this sprite to add even more pazzazz!

Here is an example of an island that has a base constructed using the sprite stacking technique and then a layer on top to draw our animated 2D sprites. If you see it in the game, these Penguins are bopping like made yokes!

![Penguins](images/penguins.png)

We can also get in small details like windows on buildings by colouring edges of sprite slices.

![City](images/city.png)
## The Programming

I won't get into this in too much detail here but I will give a couple of high level points as to how we went about this.

The engine we are using is Bevy which is open-source and written in Rust. This uses an ECS arcitecture which allowed us to get sprite stacking working with great performance.

A boat is an **entity** that holds data like the current angle within it as **components**. We then spawn children entities under that parent boat entity for each sprite slice. The **systems** then iterate quickly through each sprite slice and sets their rotation to match the boats current angle every tick.

I won't go into more detail on this right now but if some of those things sound new to you, and interesting, I recommend that you check out the [Bevy Engine](https://bevyengine.org/) as a starting point for a simple entry into using ECS.

I am super happy with the performance that I am able to achieve with it. This screenshot was taken from my 2016 (7 years ago at time of writing) laptop with just an Intel i5 and I have hundreds of boats driving around at 60 FPS!

![Swarm](images/Screenshot%202023-11-20%20230851.png)

So that's it for the moment! We are still working on [Wee Boats](https://jamespoole.itch.io/wee-boats) and will be posting updates here and on the Itch.io devlog. You can also sign up to the [newsletter](https://mailchi.mp/18efdca94182/newsletter) which I use for larger updates.

Go raibh maith agat! All the best! 

Honk
