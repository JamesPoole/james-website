---
title: Creating our own small tools to make our games
date: 2024-03-13
categories:
  - Wee Boats Devlog
---
## What's The Problem?

The Unity engine is a huge blob that I don't understand. The Godot engine is a smaller open source blob which is better but there are still lots of checkboxes to learn and scenes and nodes and stuff.

Even after learning enough to get by in these engines, I'm still only using about 20% of what they can do. The rest doesn't apply to me and is sitting there clogging up my game builds with hundreds of MB and draining me laptop battery. G'way with that.

## A Nudge in a New Direction

I was listening to an episode (that I think was taken down because I can't find it anymore) of [Gamedev Breakdown](https://podcasts.apple.com/us/podcast/gamedev-breakdown/id1262767515) where the presenter Todd Mitchel spoke about creating games from the ground up using XNA. He also spoke about a fellow called [Michael Hicks](https://michaelarts.net/) who creates his own games and also has documented some of his own self-made tools on YouTube that went into his process. 

This resonated with me a lot and sent me down a rabbit hole that gave me a bit of hope. Still, for some reason it felt a little out of reach. XNA and Windows Forms, the tools talked about mostly here, were not my comfort zone and I didn't jump head first into them.

## Enter Raylib

A little while after, I came across [Raylib](https://www.raylib.com/) which felt perfect for making game tooling. It's very similar to XNA, except in C as opposed to C#. It has a simple little API which gives lots of neat functionality like drag dropping files onto the window and drawing shapes onto the screen. Saying this, Raylib isn't the point of this post. It could be anything. It was just the piece of the jigsaw that made it seem possible for me to make my own tools. If you're on a similar buzz as me and want to try out a different tool, use whatever gets you hyped!

## My First Foray into Creating Tools

For the game that myself and my friends are working on at the moment, [Wee Boats](https://jamespoole.itch.io/wee-boats), we quickly reached a bit of a snag that was how we are creating our map. We are using a JSON config file that defines the locations of every structure in the world. This is read by our game and it constructs the world on launch. Each structure has a location associated with it that has an X and Y coordinate. Editing this in text was grand when we had two or three structures but when it went any larger than that, it was impossible to use.

Whats this? I hear a tool coming on!

## Introducing the Wee Boats Map Editor! Come on ye'boya

Using Raylib, we made a simple program that would read our JSON config and give us a visual representation of our world and easy ways to edit it.

Here is how it looks at the moment:
![Map Editor](images/Pasted%20image%2020240319205929.png)

Each circle represents a structure in the Wee Boats world and through here we can move them around, add new structures and do other common actions that we are building up over development. We can then export it and it updates the JSON config that is then read by the game itself.

Here is a small example of some of those structures in game:
![Game Example](images/Pasted%20image%2020240319210332.png)

## What are the best bits about this tool?

- I understand how it works.
- It's small. It does exactly what we need it to do and speeds us up a great deal.
- We can quickly extend it to add new functionality.
- We can quickly adapt it to work with future projects.
- It was great craic to make.

## A Small Toolier Future

Making this Map Editor and the game around it has been a blast so far. Of course I am not going to be able to build as large and amazing games as people might be able with Unity but the feeling of understanding and mastery I get from this is amazing. 

The idea of working in C freaked me out before. It sounds terrifying needing to worry about memory and all of this jazz. But that has rarely been an issue in making these. In fact, I have been moving much faster than I am used to. I feel like I have been tricked into thinking that big tools like Unity make the process easier but in reality, they have been bogging me down all along.

Us game makers don't need to be dependent on a company like Unity to be able to create work that we are proud of. Fuck them. Lets make and own our own stuff.

Maybe I am in a honeymoon phase and I am cruising for some doomed pit but where I am sitting right now, I have never felt better about making silly games.
