---
title: Sharing Scrappily
date: 2024-12-24
categories:
  - Openness
---
I need to lower the barrier to entry for publishing to this site.

[The more I share, the better.](https://sive.rs/getme)

I need to be ok with posting [scrappy](https://www.todepond.com/pondcast/demo/) writings to allow the sharing to flow and not get blocked.

Individually, a lot of the scrappy writings may be useless but hopefully, with time, they will coagulate into something useful to explore.

My favourite websites are the ones that are dense with ideas that allow me to get lost in a nest of links. My goal is to create something like that and to get as much of what I have learned out in the open as possible!

Come on in!