---
title: The Middle of the Process
date: 2024-02-11
categories:
  - Wee Boats Devlog
---
I'm hitting a tricky part of the process with Wee Boats. We have made a lot of process and have gotten to about 50%~60% of completion, I would say. 

At the start of the project, there were clear things to work on.

Then it becomes less clear as most of the elements are implemented to a certain degree.

What I am seeing as the main next parts to work on are:
1. Refactoring poorly written code.
2. Fixing bugs that have been lying around.
3. Improving poorly performing aspects of the code.
4. Improving features.

Making sure that the code is easily built upon is super important for my own motivation. If I can feel myself saying "Oh god, I'll have to tackle that" in a disheartened way, that's enough to put me off starting the job and then that's a cycle that will continue. At this point of the project, motivation is super important to me because I want to move the project forward and get it finished.

I tend to like refactoring my code. I find it rewarding as you chip away through parts of the codebase and you make things better. I'm going to take a small amount of time and put it towards improving everything that I have in front of me. Make it easier to fix bugs, improve performance and add features going forward.

I have heard people say that they refactor after every session of programming. I should probably do that a bit more.
- Sometimes lack of time makes that tricky.
- Sometimes its a higher level view when there are more pieces in place that make refactoring easier.
- Sometimes I will have learned something new after a period of time that I can go back and improve